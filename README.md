# Dutch Language Pack for Magento 2 #

This is the Bitbucket repository for the Dutch Language Pack for Magento 2 from Cream, which is available for manual installation or composer / packagist. 

# Installation #

## Via composer ##

To install this translation package with composer you need access to the command line of your server and you need to have Composer. Install the language pack with the following commands:

```
#!bash

cd <your magento path>
composer require creaminternet/language-nl_nl:@stable
php bin/magento setup:upgrade
php bin/magento cache:clean
php bin/magento setup:static-content:deploy nl_nl
```

## Manually ##

To install this language package manually you need access to your server file system and you need access to the command line of your server. And take the following steps:

*  [Download the zip file from the Bitbucket repository](https://bitbucket.org/creaminternet/language-nl_nl/downloads). 
* Upload the contents to <your magento path>/app/i18n/creaminternet/language_nl_nl.
* Execute the following commands:

```
#!bash
cd <your magento path>
php bin/magento setup:upgrade
php bin/magento cache:clean
php bin/magento setup:static-content:deploy nl_nl
```

# Usage #

## Frontend ##

To use this language pack on the frontend of the site, login to your admin panel and goto Stores -> Configuration -> General > General -> Locale options and set the 'locale' option as 'Netherlands (Dutch)'

## Backend ##

To use this language pack in the backend of Magento goto System -> Permissions -> All users. Select the user you want to change an set the 'locale' option as 'Netherlands (Dutch)' and save the user. 

# Contribute #

To help push the 'Dutch Language Pack' forward please fork the [Bitbucket repository](https://bitbucket.org/creaminternet/language-nl_nl/overview) and submit a pull request with your changes. 

# License #

This module is distributed under the following licenses:

* Academic Free License ("AFL") v. 3.0
* Open Software License v. 3.0 (OSL-3.0)

# Authors #

* This repository was started by forking the [Adwise Magento 2 Dutch Language Pack](https://github.com/Adwise/magento2-nl_NL-language)
* [Cream](https://www.cream.nl/) released this module on packagist so it can be used with composer and did additional translations.

# About Cream #

Cream is an ecommerce solution provider for the Magento platform. You'll find an overview of all our (open source) projects [on our website](https://www.cream.nl/).